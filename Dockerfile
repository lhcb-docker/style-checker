FROM debian:unstable

RUN  apt-get update \
  && apt-get install -y clang-format-8 clang-format-7 python-pip git curl \
  && pip install yapf==0.24.0 whichcraft \
  && rm -rf /var/lib/apt/lists/*
